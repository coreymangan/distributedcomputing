import java.net.*;
import java.io.*;

/**
 * A subclass of DatagramSocket which contains methods for sending and receiving
 * messages
 * 
 * @author M. L. Liu
 */

public class MyServerDatagramSocket extends DatagramSocket {
   static final int MAX_LEN = 100;

   MyServerDatagramSocket(int portNo) throws SocketException {
      super(portNo);
   }

   // Sends Message
   public void sendMessage(InetAddress receiverHost, int receiverPort, String message) throws IOException {
      byte[] sendBuffer = message.getBytes();
      DatagramPacket datagram = new DatagramPacket(sendBuffer, sendBuffer.length, receiverHost, receiverPort);
      this.send(datagram);
   }

   // Sends Message
   public void sendMessage(InetAddress receiverHost, int receiverPort, byte[] message) throws IOException {
      DatagramPacket datagram = new DatagramPacket(message, message.length, receiverHost, receiverPort);
      this.send(datagram);
   }

   // Receives Message
   public String receiveMessage() throws IOException {
      byte[] receiveBuffer = new byte[MAX_LEN];
      DatagramPacket datagram = new DatagramPacket(receiveBuffer, MAX_LEN);
      this.receive(datagram);
      String message = new String(receiveBuffer);
      return message;
   }

   // Receives Message and Sender
   public DatagramMessage receiveMessageAndSender() throws IOException {
      byte[] receiveBuffer = new byte[MAX_LEN];
      DatagramPacket datagram = new DatagramPacket(receiveBuffer, MAX_LEN);
      this.receive(datagram);
      DatagramMessage returnVal = new DatagramMessage();
      returnVal.putVal(new String(receiveBuffer), datagram.getAddress(), datagram.getPort());
      return returnVal;
   }
}
