import java.io.*;

/**
 * This module contains the application logic of an echo server which uses a
 * connectionless datagram socket for interprocess communication. A command-line
 * argument is required to specify the server port.
 * 
 * @author M. L. Liu
 */

public class Server {
   public static void main(String[] args) {
      int serverPort = 3000;
      if (args.length == 1)
         serverPort = Integer.parseInt(args[0]);
      try {
         MyServerDatagramSocket mySocket = new MyServerDatagramSocket(serverPort);
         System.out.println("Server ready.");
         // Receives incomming message and sends back response
         while (true) {
            DatagramMessage request = mySocket.receiveMessageAndSender();
            // Gets message
            String message = request.getMessage();
            System.out.println("message received: " + message);
            // Checks message for commands
            if (message != null) {
               request.CheckMessage();
            }
            // Sends back message
            mySocket.sendMessage(request.getAddress(), request.getPort(), request.getMessage());
         }
      }
      catch (Exception ex) {
         ex.printStackTrace();
      }
   }
}
