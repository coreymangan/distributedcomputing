import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.io.*;

/**
 * A class to use with MyServerDatagramSocket for returning a message and the
 * sender's address
 * 
 * @author M. L. Liu
 */
public class DatagramMessage {
   private String message;
   private InetAddress senderAddress;
   private int senderPort;
   private static String folder_name;

   private static boolean isLoggedIn;

   public void putVal(String message, InetAddress addr, int port) {
      this.message = message;
      this.senderAddress = addr;
      this.senderPort = port;
   }

   // Gets Message
   public String getMessage() {
      return this.message;
   }

   // Gets Address
   public InetAddress getAddress() {
      return this.senderAddress;
   }

   // Gets port
   public int getPort() {
      return this.senderPort;
   }

   // Checks first array of the string for correct command
   // You must first be logged in to access the other commands
   public void CheckMessage() throws IOException {
      String[] messageArray = this.message.split("\\s+");
      switch (messageArray[0].toLowerCase().trim()) {
      case "login": {
         try {
            this.login(messageArray[1], messageArray[2]);
         } catch (Exception e) {
            this.message = "Please enter login <username> <password> or register if not have account";
         }
         break;
      }
      case "logout": {
         this.logout();
         break;
      }
      case "upload": {
         this.upload(messageArray);
         break;
      }
      case "download": {
         this.download(messageArray);
         break;
      }
      case "register": {
         try {
            this.register(messageArray[1], messageArray[2]);
         } catch (Exception e) {
            this.message = "Please enter register <username> <password>";
         }
         break;
      }
      case "help": {
         System.out.println("here");
         this.message = "List of commands:\nlogin <name> <pass>\nupload <file>\ndownload <file>\nlogout\nregister <name> <pass>";
         break;
      }
      default:
         this.message = " ";
         break;
      }
   }

   // Logs in with correct username and password, then creates unique folder for user
   private void login(String username, String password) throws IOException {
      FileReader f = new FileReader("SecureFiles/Users.md");
      BufferedReader br = new BufferedReader(f);
      String line, user, pass;
      // Reads Users.md to find user credentials
      while ((line = br.readLine()) != null) {
         user = line.split(" ")[0].toLowerCase().trim();
         pass = line.split(" ")[1].toLowerCase().trim();

         // Logs user in and creates folder if user is found
         if (user.equals(username.trim()) && pass.equals(password.trim())) {
            this.message = "Logging in...";
            this.isLoggedIn = true;
            // Creates new folder, will only create if not already there
            new File(username.trim() + "_folder").mkdir();
            this.folder_name = username.trim() + "_folder";
            // Creating test file to test download command later
            new FileOutputStream(this.folder_name + "/testFile.txt").write("This is a test file".getBytes());
            break;
         } else {
            this.message = "Incorrect username or password. Are you registered?";
         }
      }
   }

   // Logs out if logged in
   private void logout() {
      if (this.isLoggedIn == true) {
         this.message = "Logging out...";
         this.isLoggedIn = false;
      } else {
         this.message = "You are not logged in. Type login <username> <password>";
      }
   }

   // Uploads file to user folder if user is logged in
   private void upload(String[] msg) {
      if (this.isLoggedIn == true) {
         try {
            String file_name = msg[1];
            FileOutputStream f = new FileOutputStream(this.folder_name + "/" + file_name);

            String[] file_content_array = Arrays.copyOfRange(msg, 2, msg.length);
            String file_content_string = String.join(" ", file_content_array).trim();
            byte[] file_content_byte = file_content_string.getBytes();

            f.write(file_content_byte);
            f.flush();
            f.close();

            this.message = "File has been successfully uploaded";
         } catch (Exception e) {
            this.message = "File not found";
         }
      } else {
         this.message = "You are not logged in. Type login <username> <password>";
      }
   }

   // Downloads file if user is logged in
   private void download(String[] msg) {
      if (this.isLoggedIn == true) {
         try {
            String file_name = msg[1];
            byte[] b = new byte[2048];
            // Creates file in users unique folder
            FileInputStream f = new FileInputStream(this.folder_name + "/" + file_name.trim());
            int i = 0;
            while (f.available() != 0) {
               b[i] = (byte) f.read();
               i++;
            }
            f.close();
            String b_as_string = new String(b);

            this.message = b_as_string;

         } catch (Exception e) {
            // If file doesnt exist, send user a message saying what txt files are available to download
            String file_list = "";
            File[] files = new File(this.folder_name + "/").listFiles(new FilenameFilter() {

               @Override
               public boolean accept(File dir, String name) {
                  return name.endsWith(".txt");
               }
            });

            for (File file : files) {
               file_list = file_list + "\n" + file.getName();
            }

            this.message = "File not found. Here are the list of txt files in server:\n" + file_list;
         }
      } else {
         this.message = "You are not logged in. Type login <username> <password>";
      }
   }

   // Registers new user
   private void register(String username, String password) throws IOException {
      FileOutputStream f = new FileOutputStream(new File("SecureFiles/Users.md"), true);
      String credentials = ("\n" + username + " " + password).trim();
      f.write("\n".getBytes());
      f.write(credentials.getBytes());
      f.close();
      this.message = "You have successfully registered. Type login <username> <password> to login";
   }
}
