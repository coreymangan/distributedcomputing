import java.net.*;
import java.nio.charset.StandardCharsets;

import java.io.*;
import java.io.File;
import java.io.FileInputStream;

/**
 * This class is a module which provides the application logic for an Echo
 * client using connectionless datagram socket.
 * 
 * @author M. L. Liu
 */
public class ClientHelper {
   private MyClientDatagramSocket mySocket;
   private InetAddress serverHost;
   private int serverPort;

   ClientHelper(String hostName, String portNum) throws SocketException, UnknownHostException {
      this.serverHost = InetAddress.getByName(hostName);
      this.serverPort = Integer.parseInt(portNum);
      this.mySocket = new MyClientDatagramSocket();
   }

   // Gets Response from server after sending message
   public String getResponse(String message) throws SocketException, IOException {
      String[] messageArray = message.split("\\s+");
      String response = "";
      byte[] msg = this.uploadFile(message);

      if (msg != null) {
         mySocket.sendMessage(serverHost, serverPort, msg);
      } else {
         mySocket.sendMessage(serverHost, serverPort, message);
      }
      response = mySocket.receiveMessage();

      // Make sure the user is logged in and the file is found before trying to download file
      if (messageArray[0].equals("download") && !response.contains("File not found")
            && !response.contains("You are not logged in")) {
         downloadFile(messageArray[1], response);
         return "File has been downloaded";
      }

      return response;
   }

   // Closes socket
   public void done() throws SocketException {
      mySocket.close();
   }

   // Checks for file and reads content, then sends file to server
   private byte[] uploadFile(String message) {
      try {
         String[] messageArray = message.split("\\s+");
         if (messageArray[0].equals("upload")) {
            byte[] b = new byte[2048];
            FileInputStream f = new FileInputStream(messageArray[1]);
            int i = 0;
            while (f.available() != 0) {
               b[i] = (byte) f.read();
               i++;
            }
            f.close();
            String b_as_string = new String(b);
            String s = new String(messageArray[0] + " " + messageArray[1] + " " + b_as_string);
            return s.getBytes();
         } else {
            return null;
         }
      } catch (Exception e) {
         return null;
      }

   }

   // Downloads file from server
   private void downloadFile(String file_name, String content) throws IOException {
      byte[] b = content.trim().getBytes();

      FileOutputStream f = new FileOutputStream(file_name);
      f.write(b);
      f.flush();
      f.close();
   }
}
