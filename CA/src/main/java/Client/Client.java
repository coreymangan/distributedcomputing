import java.io.*;

/**
 * This module contains the presentaton logic of an Echo Client.
 * 
 * @author M. L. Liu
 */
public class Client {
   static final String endMessage = ".";

   public static void main(String[] args) {
      // Readers
      InputStreamReader is = new InputStreamReader(System.in);
      BufferedReader br = new BufferedReader(is);
      try {
        
         // Inputs name of server host
         System.out.println("What is the name of the server host?(localhost)");
         String hostName = br.readLine();
         if (hostName.length() == 0)
            hostName = "localhost";

         // Inputs port number of server host
         System.out.println("What is the port number of the server host?(3000)");
         String portNum = br.readLine();
         if (portNum.length() == 0)
            portNum = "3000";
		
         ClientHelper helper = new ClientHelper(hostName, portNum);
         boolean done = false;
         String message, response;
         System.out
               .println("Enter a command to receive a response back from the server(enter help to see list of commands), " + "or a single peroid to quit.");
         // Reading line for commands
         while (!done) {
            message = br.readLine();
            if ((message.trim()).equals(endMessage)) {
               done = true;
               helper.done();
            } else {
               response = helper.getResponse(message);
               System.out.println(response);
            }
         }
      }
      catch (Exception ex) {
         ex.printStackTrace();
      }
   }
}
